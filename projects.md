---
layout: page
title: Undergraduate Projects 2019/20
---

This is a selection of projects, mostly themed around static program analysis (see e.g. [notes](https://cs.au.dk/~amoeller/spa/spa.pdf)).  The work involved in each project is part theory and part implementation.  It depends on the particular project, but generally speaking a good project will develop an analysis algorithm (by adapting work in the literature), give some argument that it is correct and evaluate it.  The best kind of evaluation is to implement the algorithm for a programming language and try it out on real programs.

If you have a good idea for a program analysis for a programming language that you are interested in that is different to these let me know and we can discuss it.

## Detecting index violations via type inference

The aim of this project is to develop and implement a refinement type system for Haskell that is able to automatically detect, at compile-time, that a given program may crash at run-time due to an indexing violation (e.g. because some input will cause it to try and select the 3rd element of a 2 element list).

For this purpose, I would adapt a refinement type system from the literature, such as Freeman and Pfennings [Refinement Types for ML](https://www.cs.cmu.edu/~fp/papers/pldi91.pdf) (but without intersection types) or the more generic (but less approachable) work of Jhala, Majumdar and Rybalchenko [HMC: Verifying Functional Programs Using Abstract Interpreters](http://goto.ucsd.edu/~rjhala/papers/hmc.pdf).  The adaptation consists of refining the type of integers by something more specific, like the family of types of integer intervals [n,m] or Logozzo and Fähndrich's [Pentagons: A Weakly Relational Abstract Domain for the Efficient Validation of Array Accesses](https://www.microsoft.com/en-us/research/wp-content/uploads/2009/01/pentagons.pdf).

## Resource usage verification for Haskell programs

The aim of this project is to implement a resource usage verifier for Haskell.  Such a static analysis will try to verify that _resources_ are used correctly.  A simple example is e.g. that all files are opened before they are read or written to.

The approach will be based on a technique from Kobayashi's [Types and Higher-Order Recursion Schemes for Verification of Higher-Order Programs](http://www-kb.is.s.u-tokyo.ac.jp/~koba/papers/popl2009.pdf) for obtaining (an abstraction of) the computation tree of a given functional program and inferring a type that will indicate whether or not the computation tree contains traces that violate proper resource usage.

## Null dereference analysis for Clojure

Clojure is a functional programming language in the Lisp family, i.e. it is based on _untyped_ λ-calculus.  Clojure is a rather modern Lisp because it is designed to run on the JVM, see e.g. https://clojure.org/about/rationale.  There is something of a community around Clojure in Bristol, perhaps because it is in use by Ovo Energy who are based here.  

The aim of this project is to implement a static analysis tool for Clojure to automatically detect, at compile-time, if the program might crash with a null dereference at run-time.  There are various interesting approaches in the literature that could be adapted:

* Steensgaard's [Points-To Analysis](https://courses.cs.washington.edu/courses/cse503/10wi/readings/steensgaard-popl96.pdf) (see also: [notes](https://www.cs.cmu.edu/~aldrich/courses/15-819O-13sp/resources/pointer.pdf))
* Jones and Andersen's [Flow Analysis](https://www.sciencedirect.com/science/article/pii/S0304397506009194)
* Shiver's 0CFA (see [notes](http://www.cs.cmu.edu/~clegoues/courses/15-819O-16sp/notes/notes08-functional-cfa.pdf) or the textbook [Principles of Program Analysis](https://bris.on.worldcat.org/oclc/42579405))
* Van Horn and Might's [Abstracting Abstract Machines](http://matt.might.net/papers/vanhorn2010abstract.pdf)

## Detecting space leaks in Haskell programs

Space leaks are where a program inadvertently retains references to data structures during some iterative process, which causes the space usage of the program to increase without bound.  There are two possible projects on this theme.  

* A research-oriented project will investigate the potential to use Lee, Jones' and Ben-Amram's [Size Change Principle for Program Termination](http://citeseerx.ist.psu.edu/viewdoc/download?doi=10.1.1.646.9176&rep=rep1&type=pdf) to detect potential space leaks statically.  The idea is to look for infinite call sequences that result in unbounded space usage.

* An implementation-oriented project will contribute to [Matthew Pickering's](https://mpickering.github.io/) [work](https://www.youtube.com/watch?v=IW2VoSdl1GM) on a GHC Debugger.  In this you would be jointly supervised by Matt.

## Program Analysis in Semmle QL

Semmle is a company, based in Oxford, that makes a suite of program analysis tools.  The big idea at Semmle is to treat the code and the program's environment - the build system, the dependencies etc - as a big database.  Finding bugs in the program, or at least suspicious looking code, is then a matter of writing a query in a special object-oriented query language called QL.  Semmle's product has been so successful that they recently got acquired by Github and the technology is likely to be integrated into Github in the near future.

In this project I want to see the extent to which we can implement some lightweight program analyses as queries in Semmle's QL.  The advantage to doing this is that we can use Semmle's technology to run the queries over hundreds of well-known open source projects, and potentially find bugs in them.

## Implementing Typed Algebraic Parser Combinators

A recent paper at PLDI proposes a new approach to parser combinators based on context free expressions [A Typed Algebraic Approach to Parsing](https://www.cl.cam.ac.uk/~nk480/parsing.pdf).  The authors have implemented a library based on their idea for OCaml.  In this project, you would implement the library for another programming language (e.g. Haskell).

